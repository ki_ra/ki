package com.ab.ethioflix.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String home() {
		return "home";	
	}
	@GetMapping("/about")
	public String about() {
		return "about";
	}
	
	@GetMapping("/discription")
	public String discription() {
		return "discription";	
	}
	@GetMapping("/tryy")
	public String tryy() {
		return "tryy";	
	}

}
