package com.ab.ethioflix.service;

import com.ab.ethioflix.domain.User;

public interface UserService {
	  
	 public User findUserByEmail(String email);
	 
	 public void saveUser(User user);
}